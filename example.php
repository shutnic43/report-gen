<?php

use Skostylev\Reports\Domain\ConfigBuilder;
use Skostylev\Reports\Domain\Enums\FileTypeEnum;
use Skostylev\Reports\Domain\Enums\ReportTypeEnum;
use Skostylev\Reports\Domain\ReportGenerationService;
use Skostylev\Reports\Infrastructure\Error;
use Skostylev\Reports\Infrastructure\TaskRepository;

/**
 * Post or Get
 */
$requestData = [
    'fileType' => 'csv',
    'reportType' => 'orders',
];

$fileType = FileTypeEnum::tryFrom($requestData['fileType']);
if (!$fileType) {
    return [
        new Error('Incorrect filetype')
    ];
}
$reportType = ReportTypeEnum::tryFrom($requestData['reportType']);

$validationErrors = [];
$config = (new ConfigBuilder())
    ->buildRequestByTypeAndRawData(
        $reportType,
        $requestData,
        $validationErrors
    );

if (!$config) {
    return $validationErrors;
}

$taskRepository = new TaskRepository();
$task = $taskRepository->registerOrFindTask($fileType, $config);

(new ReportGenerationService($taskRepository))
    ->generateByTask($task);

return ['filePath' => $task->filePath];