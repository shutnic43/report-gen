<?php

namespace Skostylev\Reports\Infrastructure\FileWriter;

final class Csv implements FileWriterInterface
{
    public function __construct(
        public readonly string $fileName,
        public readonly array  $headerColumns
    )
    {

    }

    public function write(array $row)
    {
    }

    public function openFile()
    {
    }

    public function closeFile()
    {
    }
}