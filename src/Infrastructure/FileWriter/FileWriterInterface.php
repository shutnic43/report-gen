<?php

namespace Skostylev\Reports\Infrastructure\FileWriter;

interface FileWriterInterface
{
    public function openFile();

    public function write(array $row);

    public function closeFile();
}