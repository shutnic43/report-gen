<?php

namespace Skostylev\Reports\Infrastructure\ReportCriteria;

final class ArrayCriteria extends Criteria
{
    public function __construct(
        public readonly string $name,
        public readonly array  $value,
    )
    {
        parent::__construct($this->name);
    }

    public function getValue(): array
    {
        return $this->value;
    }
}