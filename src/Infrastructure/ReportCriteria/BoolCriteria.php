<?php

namespace Skostylev\Reports\Infrastructure\ReportCriteria;

final class BoolCriteria extends Criteria
{
    public function __construct(
        public readonly string $name,
        public readonly bool   $value,
    )
    {
        parent::__construct($this->name);
    }

    public function getValue(): bool
    {
        return $this->value;
    }
}