<?php

namespace Skostylev\Reports\Infrastructure\ReportCriteria;

use DateTimeImmutable;

final class DateTimeCriteria extends Criteria
{
    public function __construct(
        public readonly string            $name,
        public readonly DateTimeImmutable $value,
    )
    {
        parent::__construct($this->name);
    }

    public function getValue(): DateTimeImmutable
    {
        return $this->value;
    }
}