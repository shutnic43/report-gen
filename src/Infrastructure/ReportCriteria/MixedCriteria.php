<?php

namespace Skostylev\Reports\Infrastructure\ReportCriteria;

final class MixedCriteria extends Criteria
{
    public function __construct(
        public readonly string $name,
        public readonly mixed  $value,
    )
    {
        parent::__construct($this->name);
    }

    public function getValue(): mixed
    {
        return $this->value;
    }
}