<?php

namespace Skostylev\Reports\Infrastructure\ReportCriteria;

abstract class Criteria
{
    public function __construct(
        public readonly string $name,
    )
    {

    }

    abstract public function getValue();
}