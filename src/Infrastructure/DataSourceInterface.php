<?php

namespace Skostylev\Reports\Infrastructure;

use Generator;

interface DataSourceInterface
{
    public function readRowByCriteria(array $criteriaCollection, int $offset): Generator;

    public function getName(): string;
}