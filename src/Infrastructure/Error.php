<?php

namespace Skostylev\Reports\Infrastructure;

class Error
{
    public function __construct(
        public readonly string $message
    )
    {

    }
}