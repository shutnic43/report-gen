<?php

namespace Skostylev\Reports\Infrastructure;

use Skostylev\Reports\Domain\Enums\FileTypeEnum;
use Skostylev\Reports\Domain\GenerationTask;
use Skostylev\Reports\Domain\ReportConfig;

final class TaskRepository
{
    public function registerOrFindTask(FileTypeEnum $fileType, ReportConfig $config): GenerationTask
    {
        $task = $this->findTaskByConfig($config);
        if ($task) {
            return $task;
        }

        return new  GenerationTask(
            'uniqueId',
            $fileType,
            $this->generateFilePathByConfig($fileType, $config),
            $config,
        );
    }

    public function getTaskById(string $id): ?GenerationTask
    {
        return null;
    }

    public function updateTask(GenerationTask $task): self
    {
        return $this;
    }

    private function generateFilePathByConfig(FileTypeEnum $fileType, ReportConfig $config): string
    {
        return '';
    }

    private function findTaskByConfig(ReportConfig $config): ?GenerationTask
    {
        /**
         * Ищем в сторе по конфигу в случае если процесс упал и запустили заново не по id
         */
        return null;
    }
}