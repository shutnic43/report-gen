<?php

namespace Skostylev\Reports\Domain\Enums;

enum TaskStatusEnum: string
{
    case OPEN = 'OPEN';
    case RUNNING = 'RUNNING';
    case END = 'END';
}