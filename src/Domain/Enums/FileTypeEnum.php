<?php

namespace Skostylev\Reports\Domain\Enums;

enum FileTypeEnum: string
{
    case Xlsx = 'xlsx';
    case Csv = 'csv';
}
