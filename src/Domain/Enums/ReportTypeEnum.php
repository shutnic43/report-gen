<?php

namespace Skostylev\Reports\Domain\Enums;

enum ReportTypeEnum: string
{
    case Orders = 'orders';
    case Customers = 'customers';
}