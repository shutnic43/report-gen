<?php

namespace Skostylev\Reports\Domain\Customers;

use Skostylev\Reports\Domain\Customers\DataSource\Unit1Datasource;
use Skostylev\Reports\Domain\ReportConfig;
use Skostylev\Reports\Infrastructure\DataSourceInterface;
use Skostylev\Reports\Infrastructure\ReportCriteria\ArrayCriteria;
use Skostylev\Reports\Infrastructure\ReportCriteria\BoolCriteria;

final class CustomerReportConfig extends ReportConfig
{
    public array $requiredFields = [
        'isActive' => BoolCriteria::class,
    ];

    public array $optionalFields = [
        'groupIds' => ArrayCriteria::class,
    ];

    protected array $headerColumns = [
        'customerId',
        'group'
    ];

    /**
     * @return DataSourceInterface[]
     */
    function getDataSources(): array
    {
        return [
            new Unit1Datasource(),
        ];
    }
}