<?php

namespace Skostylev\Reports\Domain\Customers\DataSource;

use Generator;
use Skostylev\Reports\Infrastructure\DataSourceInterface;

final class Unit1Datasource implements DataSourceInterface
{
    public function readRowByCriteria(array $criteriaCollection, int $offset): Generator
    {
        yield [1, 2, 3, 4];
        yield [2, 2, 3, 4];
    }

    public function getName(): string
    {
        return 'unit1';
    }
}