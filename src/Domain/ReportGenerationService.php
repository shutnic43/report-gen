<?php

namespace Skostylev\Reports\Domain;

use Generator;
use Skostylev\Reports\Domain\Enums\FileTypeEnum;
use Skostylev\Reports\Domain\Enums\TaskStatusEnum;
use Skostylev\Reports\Infrastructure\FileWriter\Csv;
use Skostylev\Reports\Infrastructure\FileWriter\FileWriterInterface;
use Skostylev\Reports\Infrastructure\FileWriter\Xlsx;
use Skostylev\Reports\Infrastructure\TaskRepository;

final class ReportGenerationService
{
    public function __construct(
        private readonly TaskRepository $repository = new TaskRepository(),
    )
    {

    }

    public function generateByTask(GenerationTask $task): bool
    {
        $this->repository->updateTask($task->setStatus(TaskStatusEnum::RUNNING));
        $writer = $this->makeFileWriterByType($task->filePath, $task);
        try {
            $writer->openFile();
            foreach ($this->read($task) as $item) {
                $writer->write($item);
                $this->repository->updateTask(
                    $task->setLineProcessed($task->lineProcessed + 1)
                );
            }
        } catch (\Exception $exception) {
            //@todo логгируем в случае ошибки
            $writer->closeFile();
            return false;
        }
        $writer->closeFile();
        $this->repository->updateTask($task->setStatus(TaskStatusEnum::END));
        return true;
    }

    private function read(GenerationTask $task): Generator
    {
        $config = $task->config;
        $lineProcessed = $task->lineProcessed;
        foreach ($config->getDataSources() as $dataSource) {
            if (in_array($dataSource->getName(), $task->processedDatasets, true)) {
                continue;
            }

            $this->repository->updateTask(
                $task->setCurrentDataSet($dataSource->getName())
            );
            yield $dataSource->readRowByCriteria($config->criteriaCollection, $lineProcessed);
        }
    }

    private function makeFileWriterByType(string $fileName, GenerationTask $task): FileWriterInterface
    {
        if ($task->fileType === FileTypeEnum::Xlsx) {
            return new Xlsx(
                $fileName,
                $task->config->getHeaderColumns(),
            );
        }

        return new Csv(
            $fileName,
            $task->config->getHeaderColumns(),
        );
    }
}