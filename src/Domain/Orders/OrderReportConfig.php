<?php

namespace Skostylev\Reports\Domain\Orders;

use Skostylev\Reports\Domain\Orders\DataSource\Unit1Datasource;
use Skostylev\Reports\Domain\Orders\DataSource\Unit2Datasource;
use Skostylev\Reports\Domain\ReportConfig;
use Skostylev\Reports\Infrastructure\DataSourceInterface;
use Skostylev\Reports\Infrastructure\ReportCriteria\DateTimeCriteria;
use Skostylev\Reports\Infrastructure\ReportCriteria\MixedCriteria;

final class OrderReportConfig extends ReportConfig
{
    public array $requiredFields = [
        'status' => MixedCriteria::class,
    ];

    public array $optionalFields = [
        'periodDateStart' => DateTimeCriteria::class,
        'periodDateEnd' => DateTimeCriteria::class,
    ];

    protected array $headerColumns = [
        'orderId',
        'status',
        'sum',
    ];

    /**
     * @return DataSourceInterface[]
     */
    function getDataSources(): array
    {
        return [
            new Unit1Datasource(),
            new Unit2Datasource()
        ];
    }
}