<?php

namespace Skostylev\Reports\Domain\Orders\DataSource;

use Generator;
use Skostylev\Reports\Infrastructure\DataSourceInterface;

final class Unit2Datasource implements DataSourceInterface
{
    public function readRowByCriteria(array $criteriaCollection, int $offset): Generator
    {
        yield [4, 2, 3, 4];
    }

    public function getName(): string
    {
        return 'unit2';
    }
}