<?php

namespace Skostylev\Reports\Domain;

use Skostylev\Reports\Infrastructure\DataSourceInterface;
use Skostylev\Reports\Infrastructure\ReportCriteria\Criteria;

abstract class ReportConfig
{
    /**
     * @var string[]
     */
    protected array $headerColumns = [];

    public array $requiredFields = [];

    public array $optionalFields = [];
    /**
     * @var Criteria[]
     */
    public array $criteriaCollection = [];

    /**
     * @return DataSourceInterface[]
     */
    abstract function getDataSources(): array;

    /**
     * @return string[]
     */
    function getHeaderColumns(): array
    {
        return $this->headerColumns;
    }

    public function setCriteriaCollection(array $criteriaCollection): ReportConfig
    {
        $this->criteriaCollection = $criteriaCollection;
        return $this;
    }
}