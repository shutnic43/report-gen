<?php

namespace Skostylev\Reports\Domain;

use Skostylev\Reports\Domain\Customers\CustomerReportConfig;
use Skostylev\Reports\Domain\Enums\ReportTypeEnum;
use Skostylev\Reports\Domain\Orders\OrderReportConfig;
use Skostylev\Reports\Infrastructure\Error;
use Skostylev\Reports\Infrastructure\ReportCriteria\Criteria;

final class ConfigBuilder
{
    public function buildRequestByTypeAndRawData(
        ?ReportTypeEnum $reportType,
        array           $rawData,
        array           &$validationErrors
    ): ?ReportConfig
    {
        if (!$reportType) {
            $validationErrors[] = new Error('Incorrect report type');
            return null;
        }

        $config = $this->makeConfigByType($reportType);
        if (!$config) {
            $validationErrors[] = new Error('Not found report by type');
            return null;
        }

        $validationErrors = $this->fillCriteriaFromRequestData($config, $rawData, $validationErrors);

        return $config;
    }

    /**
     * Можно вынести в отдельный конфиг
     *
     * @param ReportTypeEnum $reportType
     * @return ReportConfig|null
     */
    private function makeConfigByType(ReportTypeEnum $reportType): ?ReportConfig
    {
        return match ($reportType) {
            ReportTypeEnum::Orders => new OrderReportConfig(),
            ReportTypeEnum::Customers => new CustomerReportConfig(),
        };
    }

    private function fillCriteriaFromRequestData(
        ReportConfig $config,
        array        $rawData,
        array        $validationErrors
    ): array
    {
        /**
         * @param Criteria[] $criteriaCollection
         */
        $criteriaCollection = [];
        foreach ($config->requiredFields as $field => $criteriaClass) {
            if (!empty($rawData[$field])) {
                $criteriaCollection[] = new $criteriaClass($field, $rawData[$field]);
                continue;
            }
            $validationErrors[] = new Error('Field "' . $field . '" is empty');
        }

        foreach ($config->optionalFields as $field => $criteriaClass) {
            if (empty($rawData[$field])) {
                continue;
            }
            $criteriaCollection[] = new $criteriaClass($field, $rawData[$field]);
        }

        $config->setCriteriaCollection($criteriaCollection);
        return $validationErrors;
    }
}