<?php

namespace Skostylev\Reports\Domain;

use Skostylev\Reports\Domain\Enums\FileTypeEnum;
use Skostylev\Reports\Domain\Enums\TaskStatusEnum;

class GenerationTask
{
    public function __construct(
        public readonly string       $id,
        public readonly FileTypeEnum $fileType,
        public readonly string       $filePath,
        public readonly ReportConfig $config,
        public TaskStatusEnum        $status = TaskStatusEnum::OPEN,
        public string                $currentDataSet = '',
        public int                   $lineProcessed = 0,
        public array                 $processedDatasets = []
    )
    {

    }

    public function setCurrentDataSet(string $currentDataSet): GenerationTask
    {
        $this->currentDataSet = $currentDataSet;
        return $this;
    }

    public function setStatus(TaskStatusEnum $status): GenerationTask
    {
        $this->status = $status;
        return $this;
    }

    public function setLineProcessed(int $lineProcessed): GenerationTask
    {
        $this->lineProcessed = $lineProcessed;
        return $this;
    }
}